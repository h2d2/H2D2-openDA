package org.openda.H2D2_oDA;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Locale;
import java.util.Set;
import org.openda.blackbox.interfaces.IoObjectInterface;
import org.openda.exchange.DoublesExchangeItem;
import org.openda.exchange.timeseries.TimeSeries;
import org.openda.exchange.timeseries.TimeUtils;
import org.openda.interfaces.IExchangeItem;
import org.openda.interfaces.IPrevExchangeItem;
import org.openda.interfaces.IPrevExchangeItem.Role;

/** 
 * The H2D2_oDA class is called by OpenDA (see .xml configuration files). 
 * It implements the IoObjectInterface interface with three methods called by OpenDA: 
 *    - initialize()
 *    - getExchangeItems()
 *    - finish() 
 */
public class H2D2_oDA
  implements IoObjectInterface
{
  File workingDir;
  String fileName = null;
  String[] arguments = null;
  Integer nb_param;
  Integer nb_boundaries;
  String type;
  String[] noosLocation = null;
  String[] noosType = null;
  String ioType;
  String idpar;
  HashMap<String, String> variables = new LinkedHashMap();
  HashMap<String, IPrevExchangeItem> items = new LinkedHashMap();
  
/** 
 * The initialize() method is used to create and initialize exchangeItems. Does not return anything to OpenDA.
 * Each different type of input (input/probe, parameter, timeseries, map - not implemented) gets a different treatment
 * @param   workingDir  the directory where input files are placed
 * @param   fileName    the filename of the input file
 * @param   arguments   various arguments defined in the .xml configuration files
 */
  public void initialize(File workingDir, 
                         String fileName, 
                         String[] arguments)
  {
    // Create local copies of the input arguments, then print the input filename
    this.workingDir = workingDir;
    this.fileName = fileName;
    this.arguments = arguments;
    System.out.println("H2D2_oDA.initialize : filename = " + fileName);
    
    // Reset the nb_param,  nb_boundaries, type and ioType variables
    this.nb_param = Integer.valueOf(0);         // Number of parameters (calibration)
    this.nb_boundaries = Integer.valueOf(0);    // Number of boundary conditions (data assimilation)
    this.type = "none";
    this.ioType = "none";
    
    // Check if OpenDA arguments exist
    if (this.arguments != null) {
      // Loop on all arguments   
      for (int i = 0; i < this.arguments.length; i++) {
        // Split argument into its identifier (argsplit[0]) and its value (argsplit[1])  
        String[] argsplit = this.arguments[i].split("=");
        // Initialise and print ioType
        if (argsplit[0].equals("-ioType")) {
          this.ioType = argsplit[1];
          System.out.println("H2D2_oDA.initialize : ioType = " + this.ioType);
        // Initialise and print nb_param (if applicable)
        } else if (argsplit[0].equals("-nbPar")) {
          this.nb_param = Integer.valueOf(Integer.parseInt(argsplit[1]));
          System.out.println("H2D2_oDA.initialize : Number of parameters = " + argsplit[1]);
        // Initialise and print nb_boundaries (if applicable)
        } else if (argsplit[0].equals("-nbCnd")) {
          this.nb_boundaries = Integer.valueOf(Integer.parseInt(argsplit[1]));
          System.out.println("H2D2_oDA.initialize : Number of time series = " + argsplit[1]);  
          this.noosLocation = new String[this.nb_boundaries];
          this.noosType = new String[this.nb_boundaries];
        // Initialise and print optimization type
        } else if (argsplit[0].equals("-type")) {
          this.type = argsplit[1];
          System.out.println("H2D2_oDA.initialize : optimization type = " + argsplit[1]);
        }
      }
    // If not display an error message 
    } else {
      System.out.println("H2D2_oDA.initialize : arguments missing, please check wrapper .xml file.");
      return;
    }

    /* 
     * Check if ioType is one of the 4 possibilities :
     *  - input
     *  - parameter
     *  - timeSeries
     *  - map (not implemented)
     * If not true, print an error message
     */
    if ((!"input".equals(this.ioType)) && 
        (!"parameter".equals(this.ioType)) && 
        (!"timeSeries".equals(this.ioType)) && 
        (!"map".equals(this.ioType))) {
      System.out.println("H2D2_oDA.initialize : -ioType is not recognized, possible values are input, parameter, map or timeSeries. Please check wrapper xml file.");
      return;
    }
    
    /* 
     * Depending on the ioType, apply the appropriate method
     * ioTypes with existing initialization methods:
     *  - input
     *  - parameter
     *  - timeSeries
     */
    // If ioType in input
    if (this.ioType.equals("input"))
    {
        // Call exchangeItem initialization method for input/probe files
        this.items = initializeInput(this.items, this.fileName, this.workingDir);
    }
    // else if ioType is parameter
    else if (this.ioType.equals("parameter"))
    {
        // Determine parameter type
        if (this.type.equals("zone")) {
          this.idpar = "zone";
        } else if (this.type.equals("interp")) {
          this.idpar = "interp";
        } else if (this.type.equals("poly")) {
          this.idpar = "poly";
        } else {
          this.idpar = "par";
        }
        // Call exchangeItem initialization method for parameters
        this.items = initializeParameter(this.items, this.nb_param);
    }
    else if (this.ioType.equals("timeSeries"))
    {
        // Call exchangeItem initialization method for time series
        this.items = initializeTimeSeries(this.items, this.fileName, this.workingDir, this.nb_boundaries);
    }
  }

  
/** 
 * The getExchangeItems() method is used by OpenDA to get a list of exchange items by looping on all ids (keys)
 */
  public IPrevExchangeItem[] getExchangeItems()
  {
    int n = this.items.size();
    Set<String> keys = this.items.keySet();
    IPrevExchangeItem[] result = new IPrevExchangeItem[n];
    int i = 0;
    for (String key : keys) {
      result[i] = ((IPrevExchangeItem)this.items.get(key));
      i++;
    }
    return result;
  }
  


/** 
 * The getExchangeItems() method is used by OpenDA after it has determined new parameters or time series values
 * This method is used to write data files that can then be used by H2D2, possibly through the use of Python 
 * scripts provided in the H2D2_oDA package.
 */
  public void finish()
  {
    System.out.println("H2D2_oDA.finish(): Starting finalization step. ioType = " + this.ioType);
    
    DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
    DateFormat dateFormatFile = new SimpleDateFormat("yyyyMMdd-HHmm");
    Date date = new Date();
    
    // If the ioType is parameter
    if (this.ioType.equals("parameter"))
    {
        finishParameter(this.items, this.nb_param, this.fileName, this.workingDir, date, dateFormatFile);
    }
    // Else if ioType is timeSeries
    else if (this.ioType.equals("timeSeries"))
    {
        finishTimeSeries(this.items, this.nb_boundaries, this.fileName, this.workingDir, date, dateFormat, dateFormatFile);
    }
  }
  
  /**
   * This method adds all the input tuples [id, exchangeItem] to the method input items list 
   * @param HashMap<String, IPrevExchangeItem>  items           original input list
   * @param String                              fileName        inputfile basename
   * @param File                                workingDir      inputfile working directory
   * @return HashMap<String, IPrevExchangeItem> items           input list with the added [id, exchangeItem] tuples
   */
  public HashMap<String, IPrevExchangeItem> initializeInput(HashMap<String, IPrevExchangeItem> items,
                                                            String fileName,
                                                            File workingDir)
  {
      int integer = 1;
      boolean exist = true;
      String fileNameCache = null;
      File inputFile = null;
      HashMap<String, IPrevExchangeItem> list = items;
      
      ArrayList<IExchangeItem> inputTimeSeriesExchange = new ArrayList();
      
      while (exist) {
        fileNameCache = fileName + String.format("%03d", new Object[] { Integer.valueOf(integer) }) + ".noos";
        System.out.println("H2D2_oDA.initialize : FilenameCache = " + fileNameCache);
        inputFile = new File(workingDir, fileNameCache);
        if (!inputFile.exists()) {
          System.out.println("H2D2_oDA.initialize : " + inputFile + " does NOT exists. Total number of probes = " + String.valueOf(integer - 1));
          exist = false;
        } else {
          System.out.println("H2D2_oDA.initialize : " + inputFile + " exists.");
          double[] valuesP = null;
          double[] timeCacheP = null;
          int i = 0;
          ArrayList<String> tempTimeContainer = new ArrayList();
          ArrayList<String> tempProbeContainer = new ArrayList();
          try
          {
            FileInputStream in = new FileInputStream(inputFile);
            BufferedReader buff = new BufferedReader(new InputStreamReader(in));
            

            String line = "";
            Boolean eof = Boolean.valueOf(false);
            
            while (!eof.booleanValue()) {
              line = buff.readLine();
              if (line == null) {
                eof = Boolean.valueOf(true);
              }
              else if (line.indexOf(" ") > 0)
              {
                String[] columns = line.split(" ");
                columns[0] = columns[0].trim();

                tempTimeContainer.add(i, columns[0]);
                tempProbeContainer.add(i, columns[1]);
              }
              
              i++;
            }
            in.close();
          } catch (Exception e) {
            throw new RuntimeException("Problem reading from file " + inputFile + " : " + e.getMessage());
          }
          int n = tempTimeContainer.size();
          timeCacheP = new double[n];
          valuesP = new double[n];
          
          for (int j = 0; j < n; j++) {
            try {
              timeCacheP[j] = TimeUtils.date2Mjd((String)tempTimeContainer.get(j));
            } catch (Exception e) {
              throw new RuntimeException("Problem converting date from " + inputFile + " - check format yyyymmddHHMM : " + e.getMessage());
            }
            valuesP[j] = Double.parseDouble((String)tempProbeContainer.get(j));
          }
          
          TimeSeries TStemp = new TimeSeries(timeCacheP, valuesP);
          String id = "obs" + String.format("%03d", new Object[] { Integer.valueOf(integer) });
          TStemp.setId(id);
          list.put(id, TStemp);
          
          integer++;
        }
      }
      // Return original list with the added exchange items
      return list;
  }
  
  /**
   * This method adds all the parameter tuples [id, exchangeItem] to the method input items list 
   * @param HashMap<String, IPrevExchangeItem>  items           original input list
   * @param Integer                             nb_param        number of parameters in string format
   * @return HashMap<String, IPrevExchangeItem> items           input list with the added [id, exchangeItem] tuples
   */
  public HashMap<String, IPrevExchangeItem> initializeParameter(HashMap<String, IPrevExchangeItem> items,
                                                                Integer nb_param)
  {
      HashMap<String, IPrevExchangeItem> list = items; 
      
      double[] values0 = { 0.0D };
      double[] values1 = { 1.0D };
      

      // Loop on the number of parameter tuples
      for (int j = 0; j < nb_param.intValue(); j++)
      {
        // Create empty exchange items (will be filled by OpenDA)  
        DoublesExchangeItem exchangeItem1 = null;
        DoublesExchangeItem exchangeItem0 = null;
        
        // Add par_aXXX and par_bXXX ids with the associated empty exchange items to the input list
        exchangeItem1 = new DoublesExchangeItem("par_a" + String.format("%03d", new Object[] { Integer.valueOf(j + 1) }), IPrevExchangeItem.Role.Output, values1);
        this.items.put("par_a" + String.format("%03d", new Object[] { Integer.valueOf(j + 1) }), exchangeItem1);
        
        exchangeItem0 = new DoublesExchangeItem("par_b" + String.format("%03d", new Object[] { Integer.valueOf(j + 1) }), IPrevExchangeItem.Role.Output, values0);
        this.items.put("par_b" + String.format("%03d", new Object[] { Integer.valueOf(j + 1) }), exchangeItem0);
      }
      // Return original list with the added exchange items
      return list;
  }
  
  /**
   * This method adds all the timeSeries tuples [id, exchangeItem] to the method input items list 
   * @param HashMap<String, IPrevExchangeItem>  items           original input list
   * @param String                              fileName        inputfile basename
   * @param File                                workingDir      inputfile working directory   
   * @param Integer                             nb_boundaries   number of boundaries in integer format
   * @return HashMap<String, IPrevExchangeItem> items           input list with the added [id, exchangeItem] tuples
   */
  public HashMap<String, IPrevExchangeItem> initializeTimeSeries(HashMap<String, IPrevExchangeItem> items,
                                                                 String fileName,
                                                                 File workingDir,
                                                                 Integer nb_boundaries)
  {
        HashMap<String, IPrevExchangeItem> list = items;
        String fileNameCache = null;
        
        for (int k = 0; k < nb_boundaries.intValue(); k++)
        {
      
          File inputFile = null;
          fileNameCache = fileName + String.format("%03d", new Object[] { Integer.valueOf(k+1) }) + ".noos";
          
          inputFile = new File(workingDir, fileNameCache);
          System.out.println("H2D2_oDA.initialize : Reading time series file " + inputFile);
          if (!inputFile.exists()) {
            System.out.println("H2D2_oDA.initialize : " + inputFile + " does NOT exists. Please check boundary input filename.");
          } else {
            double[] valuesBC = null;
            double[] timeCacheBC = null;
            int i = 0;
            ArrayList<String> tempTimeContainer = new ArrayList();
            ArrayList<String> tempBCContainer = new ArrayList();
            try
            {
              FileInputStream in = new FileInputStream(inputFile);
              BufferedReader buff = new BufferedReader(new InputStreamReader(in));
              
              String line = "";
              Boolean eof = Boolean.valueOf(false);
              
              while (!eof.booleanValue()) {
                line = buff.readLine();
                
                if (line == null) {
                  eof = Boolean.valueOf(true);
                }
                else if (line.charAt(0) == '#') {
                  line = line.substring(1);
                  String[] lineSplit = line.split(":");
                  if (lineSplit[0].trim().equals("Location")) {
                    this.noosLocation[k] = lineSplit[1].trim();
                    System.out.println("H2D2_oDA.initialize : Location for boundary #" + String.format("%03d", new Object[] { Integer.valueOf(k+1) }) + " : " + this.noosLocation[k]);
                  } else if (lineSplit[0].trim().equals("Type")) {
                    this.noosType[k] = lineSplit[1].trim();
                    System.out.println("H2D2_oDA.initialize : Type for boundary #" + String.format("%03d", new Object[] { Integer.valueOf(k+1) }) + " : " + this.noosType[k]);
                  }
                } else if (line.indexOf(" ") > 0)
                {
                  String[] columns = line.split(" ");
                  columns[0] = columns[0].trim();
                  


                  tempTimeContainer.add(i, columns[0]);
                  tempBCContainer.add(i, columns[1]);
                  i++;
                }
              }
              
              in.close();
            } catch (Exception e) {
              throw new RuntimeException("Problem reading from file " + inputFile + " : " + e.getMessage());
            }
            int n = tempTimeContainer.size();
            timeCacheBC = new double[n];
            valuesBC = new double[n];
            

            for (int j = 0; j < n; j++) {
              try {
                timeCacheBC[j] = TimeUtils.date2Mjd((String)tempTimeContainer.get(j));
              } catch (Exception e) {
                throw new RuntimeException("Problem converting date from " + inputFile + " - check format yyyymmddHHMM : " + e.getMessage());
              }
              valuesBC[j] = Double.parseDouble((String)tempBCContainer.get(j));
            }

            TimeSeries TStemp = new TimeSeries(timeCacheBC, valuesBC);
            String id = "cnd" + String.format("%03d", new Object[] { Integer.valueOf(k+1) });
            TStemp.setId(id);
            list.put(id, TStemp);
            
          }
        }
        // Return original list with the added exchange items
        return list;
  }
  
  
  
  public void finishParameter(HashMap<String, IPrevExchangeItem> items,
                              Integer nb_param,
                              String fileName,
                              File workingDir,
                              Date date,
                              DateFormat dateFormat)
  {
      System.out.println("H2D2_oDA.finish(): Writing to file: " + workingDir + "/" + fileName);
      File outputFile = new File(workingDir, fileName);
      try {
        if (outputFile.isFile()) {
          outputFile.delete();
        }
      } catch (Exception e) {
        System.out.println("H2D2_oDA.finish(): trouble removing file " + fileName);
      }
      try {
        FileWriter writer = new FileWriter(outputFile);
        BufferedWriter out = new BufferedWriter(writer);

        System.out.println("H2D2_oDA.finish() : Number of parameters = " + nb_param);
        
        out.write("# List of parameters generated by OpenDA. File written on " + dateFormat.format(date) + System.getProperty("line.separator"));
        out.write("Npar = " + nb_param + System.getProperty("line.separator"));
        
        for (int j = 0; j < this.nb_param.intValue(); j++) {
          String idpar_Cache = this.idpar + String.format("%03d", new Object[] { Integer.valueOf(j + 1) });
          out.write(idpar_Cache);
          System.out.println("H2D2_oDA.finish() : Parameter name = " + idpar_Cache);
          double[] para_Cache = ((IPrevExchangeItem)items.get("par_a" + String.format("%03d", new Object[] { Integer.valueOf(j + 1) }))).getValuesAsDoubles();
          double[] parb_Cache = ((IPrevExchangeItem)items.get("par_b" + String.format("%03d", new Object[] { Integer.valueOf(j + 1) }))).getValuesAsDoubles();
          System.out.println("H2D2_oDA.finish() : Subparameter a (linear coefficient) = " + para_Cache[0]);
          System.out.println("H2D2_oDA.finish() : Subparameter b (constant part) = " + parb_Cache[0]);
          out.write("," + String.format(Locale.ROOT, "%10.9e", new Object[] { Double.valueOf(para_Cache[0]) }));
          out.write("," + String.format(Locale.ROOT, "%10.9e", new Object[] { Double.valueOf(parb_Cache[0]) }));
          out.write(System.getProperty("line.separator"));
        }
        
        out.flush();
        out.close();
        writer.close();
      }
      catch (Exception e) {
        throw new RuntimeException("H2D2_oDA.finish(): Problem writing to file " + fileName + " :" + System.getProperty("line.separator") + e.getMessage());
      }
  }
  
  public void finishTimeSeries(HashMap<String, IPrevExchangeItem> items,
                               Integer nb_boundaries,
                               String fileName,
                               File workingDir,
                               Date date,
                               DateFormat dateFormat,
                               DateFormat dateFormatFile
                               )
  {
      String fileNameCache = null;
      String fileNameCacheOld = null;
        
        for (int k = 0; k < nb_boundaries.intValue(); k++)
        {
      
          fileNameCache = fileName + String.format("%03d", new Object[] { Integer.valueOf(k+1) }) + ".noos";
          fileNameCacheOld = fileName + String.format("%03d", new Object[] { Integer.valueOf(k+1) }) + "_" + dateFormatFile.format(date) + ".noos";
          
          File outputFile = new File(workingDir, fileNameCache);
          File outputFileOld = new File(workingDir, fileNameCacheOld);
          try
          {
            if (outputFile.isFile()) {
              boolean rename = outputFile.renameTo(outputFileOld);
              if (!rename) {
                System.out.println("H2D2_oDA.finish(): trouble renaming file " + fileNameCache);
                outputFile.delete();
              }
            }
          } catch (Exception e) {
            System.out.println("H2D2_oDA.finish(): trouble removing file " + fileNameCache);
          }
          try {
            FileWriter writer = new FileWriter(outputFile);
            BufferedWriter out = new BufferedWriter(writer);
            double[] currentTimeCache = ((IPrevExchangeItem)items.get("cnd" + String.format("%03d", new Object[] { Integer.valueOf(k + 1) }))).getTimes();
            String[] currentTimeCache_readable = TimeUtils.mjdToString(currentTimeCache);
            double[] noiseCache = ((IPrevExchangeItem)items.get("cnd" + String.format("%03d", new Object[] { Integer.valueOf(k + 1) }))).getValuesAsDoubles();
            
            out.write("# Noise list generated by OpenDA. File written on " + dateFormat.format(date) + System.getProperty("line.separator"));
            out.write("# All times are GMT"+ System.getProperty("line.separator"));
            out.write("# Location : " + this.noosLocation[k] + System.getProperty("line.separator"));
            out.write("# Type : " + this.noosType[k] + System.getProperty("line.separator"));
            
            System.out.println("H2D2_oDA.finish(): Writing to file: " + workingDir + "/" + outputFile);
            
            int n = currentTimeCache.length;
            
            for (int j = 0; j < n; j++)
            {
              out.write(currentTimeCache_readable[j]);
              out.write(" " + String.format(Locale.ROOT, "%10.9e", new Object[] { Double.valueOf(noiseCache[j]) }));
              out.write(System.getProperty("line.separator"));
            }
            
            out.flush();
            out.close();
            writer.close();
          }
          catch (Exception e) {
            throw new RuntimeException("H2D2_oDA.finish(): Problem writing to file " + fileName + " :" + System.getProperty("line.separator") + e.getMessage());
          }
        }
  }
}