#!/usr/bin/env python
"""Takes 2 times as argument, and writes them as t_ini and t_fin in a format
readable by H2D2 (.inp). Date format written is %Y-%m-%dT%H:%M:%SZ
Arguments:
    - t ini (format YYYYmmddHHMM)
    - t end (format YYYYmmddHHMM)
    - output filename
"""
# -*- coding: utf-8 -*-

import argparse
import calendar
import time

parser = argparse.ArgumentParser(description='Write %1 arg and %2 as t_ini \
                                 and t_fin h2d2 commands in %3 file. \
                                 Input date format : YYYYmmddHHMM in GMT time')
parser.add_argument("t_ini", type=str, help='initialisation time')
parser.add_argument("t_end", type=str, help='end time')
parser.add_argument("t_file", type=str, help='filename')

args = parser.parse_args()

tIniEpoch = calendar.timegm(time.strptime(args.t_ini, "%Y%m%d%H%M"))
tEndEpoch = calendar.timegm(time.strptime(args.t_end, "%Y%m%d%H%M"))

tIniH2D2 = time.strftime("%Y-%m-%dT%H:%M:%SZ", time.gmtime(tIniEpoch))
tEndH2D2 = time.strftime("%Y-%m-%dT%H:%M:%SZ", time.gmtime(tEndEpoch))

outputfile = open('%s' % args.t_file, 'wt')
outputfile.write('%s%s%s\n' % ("""t_ini=sys.date.utcstrptime('""",
                               tIniH2D2, """')"""))
outputfile.write('%s%s%s\n' % ("""t_fin=sys.date.utcstrptime('""",
                               tEndH2D2, """')"""))
outputfile.close()
