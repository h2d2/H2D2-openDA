#!/usr/bin/env python
# -*- coding: utf-8 -*-

import time, calendar
import math
import argparse

def noos2cnd(inputname, outputname, prgl_in, L1, L2, sigma):

    finp = inputname
    fout = outputname
    prgl_t = prgl_in
    
    date = []
    value = []
    
    with open(finp, 'rt') as ifs:
        while True:
            l = ifs.readline()
            if not l:
                break
            else:
                if l[0] == '#':
                    try:
                        name, strTemp = l[1:].split(':')
                    except ValueError:
                        name = "null"
                else:
                    dateTemp,valueTemp = l.split()
                    dateTempNew = calendar.timegm(time.strptime(dateTemp,"%Y%m%d%H%M"))
                    date.append(dateTempNew)
                    valueTempCDF = 0.5 * (1 + math.erf(float(valueTemp)/(sigma*math.sqrt(2))))
                    value.append((L2-L1)*valueTempCDF+L1)
        
    with open('%s' % (fout), 'wt') as ofs:
        for iy in range(len(date)):
            ofs.write('%i %i %15.5f\n' % (1, len(prgl_t), date[iy]))
            
            for iz in range(0, len(prgl_t)):
                if (prgl_t[iz] == 'wind'):
                    ofs.write('%9.6e ' % (value[iy]))
                else:
                    ofs.write('%9.6e ' % prgl_t[iz])
            
            ofs.write('\n')
            
        
    print('Conversion completed!')    
    
def main():        

    parser = argparse.ArgumentParser(description='Reads and converts a .noos file to H2D2 prgl file.')
    parser.add_argument("input", type=str, help='input filename')
    parser.add_argument("output", type=str, help='output filename')
    parser.add_argument("L1", type=float, help='parameter low limit')
    parser.add_argument("L2", type=float, help='parameter high limit')
    parser.add_argument("sigma", type=float, help='OpenDA standard deviation')

    args = parser.parse_args()
    
    prgl = []
    
    prgl.append('9.8059999999999992e+000')                #  1 gravity
    prgl.append('4.2000000000000000e+001')                #  2 latitude
    prgl.append('10')                                     #  3 cw formula ! 6 - Kumar old waves / -1 - Woo relative
    prgl.append('1.0000000000000000e-006')                #  4 laminar viscosity
    prgl.append('1.0000000000000000e+000')                #  5 mixing length coef
    prgl.append('0.0000000000000000e+000')                #  6 mixing length coef related to grid size
    prgl.append('1.0000000000000000e-006')                #  7 viscosity limiter: inf
    prgl.append('1.0000000000000000e+002')                #  8 viscosity limiter: sup
    prgl.append('0.2000000000000000e-000')                #  9 dry/wet: threshold depth
    prgl.append('0.1000000000000000e-000')                # 10 dry/wet: min depth
    prgl.append('0.5000000000000000e+000')                # 11 dry/wet: manning
    prgl.append('1.0000000000000000e-000')                # 12 dry/wet: |u| max
    prgl.append('1.0000000000000000e+000')                # 13 dry/wet: porosity
    prgl.append('0.0000000000000000e+000')                # 14 dry/wet: damping
    prgl.append('1.0')                                    # 15 dry/wet: coef. convection
    prgl.append('1.0')                                    # 16 dry/wet: coef. gravity
    prgl.append('10.0')                                   # 17 dry/wet: nu diffusion   ##1/Hmin##
    prgl.append('0.0')                                    # 18 dry/wet: nu darcy
    prgl.append('1.2000000000000000e+000')                # 19 peclet           -> 1.2
    prgl.append('0.0e-004')                               # 20 damping coef.    -> 0.0
    prgl.append('0.0e-000')                               # 21 free surface smoothing (Darcy)
    prgl.append('0.2500000000000000e-005')              # 22 free surface smoothing (Lapidus)
    prgl.append('1.0')                                    # 23 coef. convection
    prgl.append('1.0')                                    # 24 coef. gravity
    prgl.append('1.0')                                    # 26 coef. manning
    prgl.append('wind')                                 # 27 coef. wind
    prgl.append('1.0')                                    # 28 coef. boundary integral
    prgl.append('1.0e-018')                               # 29 coef. penality
    prgl.append('1.0e-007')                               # 30 coef. Kt perturbation (~ 0.1*sqrt(eps_mach); eps_mach ~ 2.2e-16)
    prgl.append('1.0e-012')                               # 31 min   Kt perturbation (~ 0.1*sqrt(eps_mach); eps_mach ~ 2.2e-16)
    prgl.append('1.0')                                    # 32 coef. Kt relaxation
    prgl.append('1')                                      # 33 flag prel perturbation (0 = false, #=0 = true)
    prgl.append('1')                                      # 34 flag prno perturbation (0 = false, #=0 = true)
    
    noos2cnd(args.input,args.output,prgl,args.L1,args.L2,args.sigma)

    if args.output:
        print("output filename is {}".format(args.output))
    
if __name__ == "__main__":
    main()