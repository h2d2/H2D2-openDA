#!/usr/bin/env python
# -*- coding: utf-8 -*-

import time
import argparse

def prb2noos(inputname, outputname):

    finp = inputname
    fout = outputname
    iteration = 0
    res = 0
    time_int = 0
    
    count_conv = 0
    count_noconv = 0
    
    with open(finp, 'rt') as ifs, open(fout, 'wt') as ofs:
        while True:
            l = ifs.readline()
            if not l:
                break
            else:
                prev_iteration = iteration
                prev_res = res
                prev_time = time_int
                walltime, epoch, N1, N2, N3, solver, iteration, res = l.split()            #    reads line splits
                epoch, iteration, res = float(epoch), int(iteration), float(res)        #    converts strings to nuber of lines, columns and epoch time
                strtimes = time.strftime("%Y%m%d%H%M", time.gmtime(epoch))                #    converts epoch time to YYYYmmddHHMM format
                time_int = int(strtimes)                                                #    stores date in temp container
                
                if (time_int != prev_time):
                    if (solver == "nwtn"): 
                        if iteration == 1:
                            if (prev_iteration == 100 and
                                prev_res >= 1):
                                ofs.write('%i %i %f\n' % (time_int,0, prev_res) )
                                count_noconv = count_noconv + 1
                            else:
                                ofs.write('%i %i %f\n' % (time_int,1, prev_res) )
                                count_conv = count_conv + 1

    print(count_noconv)
    print(count_conv)
    print(count_noconv+count_conv)
    print('Conversion completed!')    
    
        

parser = argparse.ArgumentParser(description='Reads and converts a .prb files to N (number of probes) .noos files.')
parser.add_argument("input", type=str, help='input .prb filename')
parser.add_argument("output", type=str, help='output basename')

args = parser.parse_args()
print "input filename is {}".format(args.input)

if args.output:
    print "output filename(s) is(are) {}XX.noos".format(args.output)
    
if __name__ == "__main__":
    if args.output:
        prb2noos(args.input,args.output)
    else:
        prb2noos(args.input,args.input)
        


