#!/usr/bin/env python
# -*- coding: utf-8 -*-

finp = 'simul000.pst.prb'
fout = 'simul000.pst.prb.csv'

with open(finp, 'rt') as ifs, open(fout, 'wt') as ofs:
    while True:
        toks = []

        l = ifs.readline()
        nlin, ncol, time = l.split()
        nlin, ncol, time = int(nlin), int(ncol), float(time)
        toks.append(time)
        for iv in range(nlin):
            l = ifs.readline()
            v = float(l)
            toks.append(v)
     
        for v in toks:
            ofs.write(' %f;' % v)
        ofs.write('\n')