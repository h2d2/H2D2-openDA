#!/usr/bin/env python
"""Reads and converts a .prb file to N .noos files.
Input arguments are:
   - input filename
   - -o=output basename (optional, the inputfile name will be used if
                         not provided)
One .noos file is written per probe. 'XXX.noos' is added to the basename
Date format in the output files is YYYYmmddHHMM
"""
# -*- coding: utf-8 -*-

import time
import argparse

def cndOutputListGenerator(inputName, outputName):
    """Reads the first line of a .prb file to determine the number of necessary
    .noos files.
    Creates the corresponding filename (basename + XXX + .noos), opens the file
    and returns the list of open files.
    """
    # creates empty list of output files
    outputList = []

    # Open .cnd input file to create a list of output filenames
    with open(inputName, 'rt') as ifs:
        #   read line
        line = ifs.readline()
        # Read line splits
        nLin, nCol, times = line.strip().split(None, 3)
        # Convert strings to number of lines (int) and epoch time (float)
        nLin, nCol, times = int(nLin), int(nCol), float(times)
        #   creates the list of output filenames
        for iw in range(nLin):
            # output filename = basename + XXX + .noos
            filename = outputName + str(iw+1).zfill(3) + '.noos'
            # open output file
            fileoutput = open(filename, 'wt')
            # add output file to the list
            outputList.append(fileoutput)

    return(outputList)

def prb2noos(inputName, outputList):
    """Converts a single .prb file (H2D2 format) to N .noos files.
    Date format in .noos files is YYYYmmddHHMM
    """
    # Open .prb file
    with open(inputName, 'rt') as ifs:
        # Loop on all inputfile lines
        while True:
            line = ifs.readline()
            # If line does not exist, break loop
            if not line:
                break
            else:
                # Read line splits
                nLin, nCol, times = line.strip().split(None, 3)
                # Convert strings to number of lines (int), number of columns
                # (int) and epoch time (float)
                nLin, nCol, times = int(nLin), int(nCol), float(times)
                # Convert epoch time to YYYYmmddHHMM format
                strTimeOut = time.strftime("%Y%m%d%H%M", time.gmtime(times))
                # Store date in temporary container
                timeOut = int(strTimeOut)

                for iv in range(nLin):
                    prbValue = ifs.readline().strip()
                    # Write time and probe value
                    outputList[iv].write('%i %s\n' % (timeOut, prbValue))

    print('Conversion completed!')

def closeOutputList(fileList):
    """Closes all open files in the list provided
    """
    for filename in fileList:
        filename.close()

def main():
    """Reads and converts a .prb file to N .noos files.
    Input arguments are:
        - input filename
        - -o=output basename (optional, the inputfile name will be used if
                              not provided)
    One .noos file is written per probe. 'XXX.noos' is added to the basename
    Date format in the output files is YYYYmmddHHMM
    """
    parser = argparse.ArgumentParser(description='Reads and converts \
                                     a .prb files to N (number of probes) \
                                     .noos files.')
    parser.add_argument("input",
                        type=str,
                        help='input H2D2 probe filename')
    parser.add_argument("-o",
                        "--output",
                        type=str,
                        help='output basename')

    args = parser.parse_args()
    print("input filename is {}".format(args.input))

    if args.output:
        print("output filename(s) is(are) {}XXX.noos".format(args.output))

    # If the output basename is provided, use it
    if args.output:
        # Generate output files list
        outputList = cndOutputListGenerator(args.input, args.output)
        # Convert .prb to .noos
        prb2noos(args.input, outputList)
        # Close output files
        closeOutputList(outputList)
    # Otherwise, use the input filename as basename for the output files
    else:
        outputList = cndOutputListGenerator(args.input, args.input)
        prb2noos(args.input, outputList)
        closeOutputList(outputList)

if __name__ == "__main__":
    main()
