#!/usr/bin/env python
"""Reads and converts N .noos files to a H2D2 .cnd file.
Input arguments are:
    - input basename
    - output filename
    - -n number of boundary .noos files (optional - default 1)
The .noos files have the location and type of the boundary in the header
'XXX.noos' is added to the basename for the inputfiles.
Date format in the input files is YYYYmmddHHMM
"""
# -*- coding: utf-8 -*-

import time
import calendar
import argparse

def noos2cnd(inputName, outputName, nbInput):
    """Converts N .noos files to a single H2D2 .cnd file.
    Date format in .noos files is YYYYmmddHHMM
    The location (variable name) and type of the condition must be present
    at the beginning of the .noos file to be written back in the .cnd file.
    """
    # Create empty lists for the boundary Location, Type, Date and Values
    cndLocation = []
    cndType = []
    cndDate = []
    cndValue = []

    # Loop on the number of boundaries
    for iw in range(nbInput):
        # Create empty sublist for the date and associated value
        cndDate.append([])
        cndValue.append([])
        # Input filename = basename + XXX.noos
        inputFilename = inputName + str(iw+1).zfill(3) + '.noos'
        # Open .noos file
        with open(inputFilename, 'rt') as ifs:
            # Loop on all lines
            while True:
                line = ifs.readline()
                # If line does not exist, break loop
                if not line:
                    break
                else:
                    # If first character is # -> Location or type
                    if line[0] == '#':
                        try:
                            # Store name and value
                            name, strTemp = line[1:].split(':')
                        except ValueError:
                            name = "null"
                        # If name is Location, store value in cndLocation
                        if name.strip() == 'Location':
                            cndLocation.append(strTemp.strip())
                        # Else if name is Type, store value in cndType
                        elif name.strip() == 'Type':
                            cndType.append(strTemp.strip())
                    # Otherwise, format is date, boundary value
                    else:
                        # Read line splits
                        dateTemp, valueTemp = line.split()
                        # Convert YYYYmmddHHMM to Python time
                        dateTime = time.strptime(dateTemp, "%Y%m%d%H%M")
                        # Convert time to epoch
                        dateEpoch = calendar.timegm(dateTime)
                        # Store epoch time
                        cndDate[iw].append(dateEpoch)
                        # Store corresponding value
                        cndValue[iw].append(float(valueTemp))

    # Open output file
    with open('%s' % (outputName), 'wt') as ofs:
        # Loop on all date values from the first boundary file
        for iy in range(len(cndDate[0])):
            # Write the number of boundaries and the date
            ofs.write('%i %15.5f\n' % (nbInput, cndDate[0][iy]))
            # Loop on each boundary
            for iz in range(nbInput):
                # Write the location, the Type and the Value
                ofs.write('%s %s %15.10e\n' % (cndLocation[iz],
                                               cndType[iz],
                                               cndValue[iz][iy]))

    print('Conversion completed!')

def main():
    """Reads and converts N .noos files to a H2D2 .cnd file.
    Input arguments are:
        - input basename
        - output filename
        - -n number of boundary .noos files (optional - default 1)
    The .noos files have the location and type of the boundary in the header
    'XXX.noos' is added to the basename for the inputfiles.
    Date format in the input files is YYYYmmddHHMM
    """
    parser = argparse.ArgumentParser(description='Reads and converts .noos \
                                     files to a H2D2 input file \
                                     (boundary condition).')
    parser.add_argument("input", type=str, help='input filename/basename')
    parser.add_argument("output", type=str, help='output filename')
    parser.add_argument("-n",
                        "--nbInput",
                        type=str,
                        help='Number of input files. XXX.noos will be added \
                        to the input basename.')

    args = parser.parse_args()

    if args.nbInput:
        nbInput = int(args.nbInput)
        print("input filenames are {}XXX.noos".format(args.input))
    else:
        nbInput = 1

    noos2cnd(args.input, args.output, nbInput)

    print("output filename is {}".format(args.output))

if __name__ == "__main__":
    main()
    