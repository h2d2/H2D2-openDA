#-------------------------------------------------------------------
# Summary:  Builds the nodal properties for sv2d
#
# Description:
#     The function build_prno_sv2d builds the mnt (topography, manning,
#     ice and wind) from the files in mnt_dir.
#
# Input:
#     mnt_dyn_dir     Directory containing all necessary file to construct
#                     the numerical terrain model (man, gla, vnt)
#     mnt_stat_dir    Directory containing all necessary file to construct
#                     the numerical terrain model (bth)
#
# Output:
#     h_prn       Handle on the constructed nodal properties.
#
# Notes:
#-------------------------------------------------------------------
function build_prno_sv2d(mnt_dyn_dir, mnt_stat_dir)

    assert_symbol(mnt_dyn_dir,'build_prno_sv2d: Undefined symbol: mnt_dyn_dir')
    assert_symbol(mnt_stat_dir,'build_prno_sv2d: Undefined symbol: mnt_stat_dir')

    #---  Topography
    h_vno_bth  = vnod(mnt_stat_dir+'mnt_sv2d.bth')

    #--- Total Manning friction
    #h_vno_frs  = vnod(mnt_dyn_dir+'mnt_sv2d.man')
    #h_vno_frs = vnod(build_manning_list(), 'r')
    h_vno_frs = vnod('/share/data2/gosselre/SHOP/data/manning', 'mnt_sv2d.man', 1, 'r')

    #---  Nodal properties
    #h_vno_ice  = vnod(mnt_dyn_dir+'mnt_sv2d_noice.gla')
    #h_vno_win  = vnod(mnt_dyn_dir+'mnt_sv2d.vnt')
    #h_vno_ice = vnod(build_ice_list(), 'r')
    h_vno_ice = vnod('/share/data2/gosselre/SHOP/data/ice', 'mnt_sv2d.gla', 1, 'r')
    #h_vno_win = vnod(build_wind_list(), 'r')
    h_vno_win = vnod('/share/data2/gosselre/SHOP/data/wind', 'mnt_sv2d.vnt', 1, 'r')

    #---  Regroup the nodal values as nodal properties
    h_prn = prno(h_vno_bth, h_vno_frs, h_vno_ice, h_vno_win)

    build_prno_sv2d = h_prn
endfunction

#----------------------------------------------------------------------------------
# Summary:  Builds the nodal properties for cd2d
#
# Description:
#     The function build_prno_cd2d builds the mnt (velocity, depth, diffusivities)
#     from the files in mnt_dir.
#
# Input:
#     mnt_dir     Directory containing all necessary file to construct the numerical
#                 terrain model.
#
# Output:
#     h_prn       Handle on the constructed nodal properties.
#
# Notes:
#------------------------------------------------------------------------------------
function build_prno_cd2d(mnt_dir) # A REVISER

    assert_symbol(mnt_dir,'build_prno_cd2d: Undefined symbol: mnt_dir')

    #---  Velocity
    h_vno_vel = vnod(mnt_dir+'mnt.vel')

    #---  Depth
    h_vno_dep = vnod(mnt_dir+'mnt.dep')

    #---  Horizontal and vertical diffusivity
    h_vno_dff = vnod(mnt_dir+'mnt.dff')

    #---  Regroup the nodal values as nodal properties
    h_prn = prno(h_vno_vel, h_vno_dep, h_vno_dff)

    build_prno_cd2d = h_prn
endfunction

#----------------------------------------------------------------------------------
# Summary:  Builds the cd2d nodal properties from sv2d
#
# Description:
#     The function build_prno_cd2d_from_sv2d builds the nodal properties for a cd2d
#     simulation from sv2d nodal values.
#
# Input:
#     h_vno_link     Handle on the sv2d nodal values
#
# Output:
#     h_prn          Handle on the constructed nodal properties.
#
# Notes:
#------------------------------------------------------------------------------------
function build_prno_cd2d_from_sv2d(h_vno_link)

    assert_symbol(h_vno_link,'build_prno_cd2d_from_sv2d: Undefined symbol: h_vno_link')

    #id_ux = 1         # Velocity in x
    #id_uy = 2         # Velocity in y
    #id_H  = 5         # Depth
    #id_dh = 9         # Horizontal diffusivity
    #id_dv = 8         # Vertical diffusivity

    id_ux =1       # Velocity in x
    id_uy = 2         # Velocity in y
    id_H  = 3         # Depth
    id_dh =  4        # Horizontal diffusivity
    id_dv = 5        # Vertical diffusivity

    h_prno = prno()
    h_prno.add(h_vno_link, id_ux, id_uy, id_H, id_dh, id_dv)
 

    build_prno_cd2d_from_sv2d = h_prno
endfunction

#----------------------------------------------------------------------------------
# Summary:  Builds the nodal properties for cd2d
#
# Description:
#     The function build_prno_cd2d builds the mnt (velocity, depth, diffusivities)
#     from the files in mnt_dir.
#
# Input:
#     mnt_dir     Directory containing all necessary file to construct the numerical
#                 terrain model.
#
# Output:
#     h_prn       Handle on the constructed nodal properties.
#
# Notes:
#------------------------------------------------------------------------------------
function build_prno_cd2d_2(mnt_dir) # A REVISER

    assert_symbol(mnt_dir,'build_prno_cd2d: Undefined symbol: mnt_dir')

    #---  Velocity
    h_vno_vx = vnod(mnt_dir+'c2d2.vx')
    h_vno_vy = vnod(mnt_dir+'c2d2.vy')

    #---  Depth
    h_vno_dep = vnod(mnt_dir+'c2d2.prof')

    #---  Horizontal and vertical diffusivity

    h_vno_hori = vnod(mnt_dir+'c2d2.difuhori')
    h_vno_dff = vnod(mnt_dir+'c2d2.difuvert')


    #---  Regroup the nodal values as nodal properties

    h_prn = prno(h_vno_vx,h_vno_vy, h_vno_dep,h_vno_hori,h_vno_dff)

    build_prno_cd2d_2 = h_prn
endfunction
