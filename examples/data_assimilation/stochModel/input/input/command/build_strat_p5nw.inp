#-------------------------------------------------------------------
# Summary:  Builds the strategy for sv2d
#
# Description:
#     The function build_strategy_sv2d builds the resolution strategy
#     for the non-linear sv2d problem.
#
# Input:
#
# Output:
#     h_solver_sv2d       Handle on the constructed solver
#
# Notes:
#-------------------------------------------------------------------
function build_strategy_sv2d(h_simd, l_gmres, nb_proc, l_transient)

    #------------------------------------
    #  Stopping criteria, limiter
    #------------------------------------
    h_lim  = glob_limiter(h_simd, 1.0e+00, 1.0e+00, 0.50e+00)
    h_cria = cria_infinity_allrel(h_simd,                   # du < (rel*|u| + abs)
                                  0.0e-08,                 # relatif
                                  1.0000000000000000e-004) # absolu
                              
    #------------------------------------
    #  Resolution algorithm
    #------------------------------------
    if (l_gmres == 1)
        nprec = 20
        nrdem = 5
        niter = 20
        eps0  = 1.0e-06    # eps0 + (eps11 + eps12*eta)*res
        eps11 = 1.0e-02
        eps12 = 0.0
        h_matslv = superlu_sp() 
        h_precond = pr_slvr(h_matslv)
        h_btrk   = backtracking_2(0.001, 1)
        h_nlnslv = gmres(h_btrk, h_precond, nprec, nrdem, niter, eps0, eps11, eps12)
        h_sttgy = h_nlnslv
    else
        h_btrk0 = 0



        h_btrk_nwtn = backtracking_2(0.1, 2)
        h_glob_nwtn = glob_compose(h_btrk_nwtn, h_lim)
        h_btrk_pcrd = backtracking_2(0.1, 2)
        h_glob_pcrd = glob_compose(h_btrk_pcrd, h_lim)
        h_btrk3 = backtracking_3(0.1, 1)
  
        if (nb_proc == 1)      
            h_matslv = mumps_linear_solver()
        else
            h_matslv = mumps_linear_solver()
           
        endif
        
        h_pcrd1_bkt = picard(h_cria, h_glob_pcrd, h_matslv, 1)
        h_pcrd10_bkt = picard(h_cria, h_glob_pcrd, h_matslv, 10)
        h_pcrd15_bkt = picard(h_cria, h_glob_pcrd, h_matslv, 15)
        h_pcrd25_bkt = picard(h_cria, h_glob_pcrd, h_matslv, 25)
        h_nwtn05 = newton(h_cria, h_glob_nwtn, h_matslv, 5)
        h_nwtn10 = newton(h_cria, h_glob_nwtn, h_matslv, 10)

        #Stratégies de résolution


        h_node5 = algo_node (h_pcrd25_bkt, 0, 0, h_nwtn05, 0)
        h_node6 = algo_node_for(h_node5, 2, 0, 0, 0, 0)
        h_node7 = algo_node (h_nwtn10, 0, 0, h_node6, 0)
        h_stgy4 = h_node7


        h_node2 = algo_node (h_pcrd25_bkt, 0, 0, h_stgy4, 0)
        h_node1 = algo_node_for(h_node2, 2, 0, 0, 0, 0)
        h_node0 = algo_node (h_pcrd15_bkt, 0, 0, h_node2, 0)
        h_stgy1 = h_node0


        h_node4 = algo_node (h_nwtn10, 0, 0, h_stgy1, 0)
        h_node3 = algo_node (h_pcrd10_bkt, 0, 0, h_node4, 0)
        h_stgy2 = h_node3


        h_node_pic=algo_node (h_nwtn10, 0, 0, h_stgy2, 0)
        h_node_n = algo_node_for(h_node_pic, 2, 0, 0, 0, 0)
        h_node_n2=algo_node (h_pcrd15_bkt, 0, 0, h_node_pic, 0)
        h_stgy3=h_node_n2

    endif

    #----------------------------------
    #  Time integration
    #-----------------------------------
    if (l_transient == 0)
        h_solver = h_stgy3
    else
        delt  = 5*60                             
        #   nitr  = 5
        h_euler2  = euler(h_stgy1,  delt/2, 1)   # Implicite
        h_euler1  = euler(h_stgy2,  delt, 1)     # Implicite
        h_euler0  = euler(h_stgy1,  delt, 1)     # Implicite
        #   h_runge_kutta = runge_kutta_4l(nitr,delt)

        h_node_eulr2  = algo_node_bi(h_euler2, 2, 0, 0, 0, 0)
        h_node_eulr1  = algo_node   (h_euler1, 0, 0, h_node_eulr2, 1)
        h_node_eulr0  = algo_node   (h_euler0, 0, 0, h_node_eulr2, 1)
        h_solver = h_node_eulr0
        #   h_solver = h_runge_kutta
    endif

    build_strategy_sv2d = h_solver
endfunction


#-------------------------------------------------------------------
# Summary:  Builds the strategy for cd2d
#
# Description:
#     The function build_strategy_cd2d builds the resolution strategy
#     for the linear cd2d problem.
#
# Input:
#
# Output:
#     h_solver_sv2d       Handle on the constructed solver
#
# Notes:
#-------------------------------------------------------------------
function build_strategy_cd2d(h_simd, nb_proc, l_transient)
    h_cria = cria_infinity_allrel (h_simd,       # du < (rel*|u| + abs)
                              0.0e-008,     # relatif
                              1.0e-004)     # absolu
    h_btrk = 0
    h_glob_limiter = glob_limiter(h_simd, 1.0e+10)
    # h_glob_compose = glob_compose(h_glob_limiter, h_btrk)
    niter = 1
    #h_matslv = superlu_sp ()


    if (nb_proc == 1)
        h_matslv = mumps_linear_solver()
    else
        h_matslv = mumps_linear_solver()
    endif
    
if (l_transient == 0)      
        h_linslv = picard (h_cria, h_glob_limiter, h_matslv, niter)
        h_solver = h_linslv 
    else 
        h_newton = newton (h_cria, h_glob_limiter, h_matslv, niter)
        #---  Time integration
        #delt = 5*60
        h_euler = euler (h_newton, 1.0, 0.5)   # Crank-Nicholson
        h_solver = h_euler
    endif

    build_strategy_cd2d = h_solver

endfunction
