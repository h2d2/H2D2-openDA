#!/usr/bin/env python
# -*- coding: utf-8 -*-

import time
import argparse

def prb2noos(inputname, outputname):

	finp = inputname
	fout = outputname

	ofs = []
	with open(finp, 'rt') as ifs:
		l = ifs.readline()
		nlin, ncol, times = l.split()											#	reads line splits
		nlin, ncol, times = int(nlin), int(ncol), float(times)					#	converts strings to nuber of lines, columns and epoch time
		for iw in range(nlin):
			fileoutput=open('%s%02i%s' % (fout,iw+1,'.noos'), 'wt')
			ofs.append(fileoutput)

	with open(finp, 'rt') as ifs:
		while True:
			l = ifs.readline()
			if not l:
				break
			else:
				nlin, ncol, times = l.split()											#	reads line splits
				nlin, ncol, times = int(nlin), int(ncol), float(times)					#	converts strings to nuber of lines, columns and epoch time
				strtimes=time.strftime("%Y%m%d%H%M", time.gmtime(times))				#	converts epoch time to YYYYmmddHHMM format
				time_int = int(strtimes)												#	stores date in temp container
				
				for iv in range(nlin):
					l = ifs.readline().strip()
					ofs[iv].write('%i %s\n' % (time_int,l) )
#					ofs[iv].write('%f %s\n' % (times,l) )
		
	for of in ofs:
		of.close
		
	print('Conversion completed!')	
	
		

parser = argparse.ArgumentParser(description='Reads and converts a .prb files to N (number of probes) .noos files.')
parser.add_argument("input", type=str, help='input .prb filename')
parser.add_argument("-o", "--output", type=str, help='output basename')

args = parser.parse_args()
print "input filename is {}".format(args.input)

if args.output:
	print "output filename(s) is(are) {}XX.noos".format(args.output)
	
if __name__ == "__main__":
	if args.output:
		prb2noos(args.input,args.output)
	else:
		prb2noos(args.input,args.input)
		


