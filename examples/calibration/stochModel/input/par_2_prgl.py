#!/usr/bin/env python
# -*- coding: utf-8 -*-

import math
import argparse
from scipy.special import erfinv

def par2prgl(inputname, outputname, prgl_in):

    finp = inputname
    fout = outputname
    prgl_t = prgl_in
    
    value = []
    
    with open(finp, 'rt') as ifs:
        while True:
            l = ifs.readline()
            if not l:
                break
            else:
                if l[0:4] == 'Npar':
                    try:
                        _, nPar = l.split('=')
                        if float(nPar) != 5:
                            print('Error, check number of parameters!')
                            break
                    except ValueError:
                        nPar = 0
                elif l[0:6] == 'par001':                                                                    #   hmin
                    name, valueATemp, valueBTemp = l.split(',')
                    L1 = 0.0
                    L2 = 0.5
                    sigma = 0.1
                    valIni = 0.1
                    mu = valIni - sigma*math.sqrt(2)*erfinv(2*(valIni-L1)/(L2-L1)-1)
                    valueBTempCDF = 0.5 * (1 + math.erf((float(valueBTemp)-mu)/(sigma*math.sqrt(2))))
                    value.append((L2-L1)*valueBTempCDF+L1)
                elif l[0:6] == 'par002':                                                                    #   htrig/hmin
                    name, valueATemp, valueBTemp = l.split(',')
                    L1 = 1.1
                    L2 = 5
                    sigma = 0.1
                    valIni = 2.0
                    mu = valIni - sigma*math.sqrt(2)*erfinv(2*(valIni-L1)/(L2-L1)-1)
                    valueBTempCDF = 0.5 * (1 + math.erf((float(valueBTemp)-mu)/(sigma*math.sqrt(2))))
                    value.append((L2-L1)*valueBTempCDF+L1)
                elif l[0:6] == 'par003':                                                                    #   manning
                    name, valueATemp, valueBTemp = l.split(',')
                    L1 = 0.05
                    L2 = 1.0
                    sigma = 0.1
                    valIni = 0.5
                    mu = valIni - sigma*math.sqrt(2)*erfinv(2*(valIni-L1)/(L2-L1)-1)
                    valueBTempCDF = 0.5 * (1 + math.erf((float(valueBTemp)-mu)/(sigma*math.sqrt(2))))
                    value.append((L2-L1)*valueBTempCDF+L1)
                elif l[0:6] == 'par004':                                                                    #   Peclet
                    name, valueATemp, valueBTemp = l.split(',')
                    L1 = 0.4
                    L2 = 1.0
                    sigma = 0.1
                    valIni = 0.5
                    mu = valIni - sigma*math.sqrt(2)*erfinv(2*(valIni-L1)/(L2-L1)-1)
                    valueBTempCDF = 0.5 * (1 + math.erf((float(valueBTemp)-mu)/(sigma*math.sqrt(2))))
                    value.append((L2-L1)*valueBTempCDF+L1)
                elif l[0:6] == 'par005':                                                                    #   Darcy
                    name, valueATemp, valueBTemp = l.split(',')
                    L1 = 0.0
                    L2 = 2.0
                    sigma = 0.1
                    valIni = 1.0
                    mu = valIni - sigma*math.sqrt(2)*erfinv(2*(valIni-L1)/(L2-L1)-1)
                    valueBTempCDF = 0.5 * (1 + math.erf((float(valueBTemp)-mu)/(sigma*math.sqrt(2))))
                    value.append((L2-L1)*valueBTempCDF+L1)
#                elif l[0:6] == 'par006':                                                                    #   KT min
#                    name, valueATemp, valueBTemp = l.split(',')
#                    L1 = 0.0
#                    L2 = 16.0
#                    sigma = 3.0
#                    valIni = 12.0
#                    mu = valIni - sigma*math.sqrt(2)*erfinv(2*(valIni-L1)/(L2-L1)-1)
#                    valueBTempCDF = 0.5 * (1 + math.erf((float(valueBTemp)-mu)/(sigma*math.sqrt(2))))
#                    value.append((L2-L1)*valueBTempCDF+L1)
#                elif l[0:6] == 'par007':                                                                    #   KT delta
#                    name, valueATemp, valueBTemp = l.split(',')
#                    L1 = 0.0
#                    L2 = 16.0
#                    sigma = 3.0
#                    valIni = 7.0
#                    mu = valIni - sigma*math.sqrt(2)*erfinv(2*(valIni-L1)/(L2-L1)-1)
#                    valueBTempCDF = 0.5 * (1 + math.erf((float(valueBTemp)-mu)/(sigma*math.sqrt(2))))
#                    value.append((L2-L1)*valueBTempCDF+L1)
        
    with open('%s' % (fout), 'wt') as ofs:
        ofs.write('%i %i %15.5f\n' % (1, len(prgl_t), 0))
            
        for iz in range(0, len(prgl_t)):
            if (prgl_t[iz] == 'dw_threshold'):
                print(('dw_threshold = {}'.format(value[0]*value[1])))
                ofs.write('%9.6e ' % (value[0]*value[1]))
            elif (prgl_t[iz] == 'dw_min'):
                print(('dw_min = {}'.format(value[0])))
                ofs.write('%9.6e ' % (value[0]))
            elif (prgl_t[iz] == 'dw_manning'):
                print(('dw_manning = {}'.format(value[2])))
                ofs.write('%9.6e ' % (value[2]))
            elif (prgl_t[iz] == 'dw_peclet'):
                print(('dw_peclet = {}'.format(value[3])))
                ofs.write('%9.6e ' % (value[3]))   
            elif (prgl_t[iz] == 'dw_darcy'):
                print(('dw_darcy = {}'.format(value[4])))
                ofs.write('%9.6e ' % (value[4])) 
#            elif (prgl_t[iz] == 'KT_per'):
#                print(value[5])
#                print(value[6])
#                print('KT_per = {}'.format(10**(-value[5])+10**(-value[6])))
#                ofs.write('%9.6e ' % (10**(-value[5])+10**(-value[6]))) 
#            elif (prgl_t[iz] == 'KT_min'):
#                print('KT_min = {}'.format(10**(-value[5])))
#                ofs.write('%9.6e ' % (10**(-value[5]))) 
            else:
                ofs.write('%s ' % prgl_t[iz])
        
        ofs.write('\n')
            
        
    print('Conversion completed!')    
    
def main():        

    parser = argparse.ArgumentParser(description='Reads and converts parameters from OpenDA to H2D2 global properties file.')
    parser.add_argument("input", type=str, help='input filename')
    parser.add_argument("output", type=str, help='output filename')

    args = parser.parse_args()
    
    prgl = []
    
    prgl.append('9.8059999999999992e+000')                #  1 gravity
    prgl.append('4.2000000000000000e+001')                #  2 latitude
    prgl.append('6')                                      #  3 cw formula ! 6 - Kumar old waves / -1 - Woo relative
    prgl.append('1.0000000000000000e-006')                #  4 laminar viscosity
    prgl.append('1.0000000000000000e+000')                #  5 mixing length coef
    prgl.append('0.0000000000000000e+000')                #  6 mixing length coef related to grid size
    prgl.append('1.0000000000000000e-006')                #  7 viscosity limiter: inf
    prgl.append('1.0000000000000000e+002')                #  8 viscosity limiter: sup
    prgl.append('dw_threshold')                           #  9 dry/wet: threshold depth
    prgl.append('dw_min')                                 # 10 dry/wet: min depth
    prgl.append('dw_manning')                             # 11 dry/wet: manning
    prgl.append('1.0000000000000000e-000')                # 12 dry/wet: |u| max
    prgl.append('1.0000000000000000e+000')                # 13 dry/wet: porosity
    prgl.append('0.0000000000000000e+000')                # 14 dry/wet: damping
    prgl.append('1.0')                                    # 15 dry/wet: coef. convection
    prgl.append('1.0')                                    # 16 dry/wet: coef. gravity
    prgl.append('1.0e-003')                               # 17 dry/wet: nu visco
    prgl.append('dw_peclet')                              # 18 dry/wet: Peclet
    prgl.append('dw_darcy')                               # 19 dry/wet: nu darcy
    prgl.append('1.0000000000000000e+000')                # 20 peclet           -> 1.2
    prgl.append('0.0e-004')                               # 21 damping coef.    -> 0.0
    prgl.append('0.0e-000')                               # 22 free surface smoothing (Darcy)
    prgl.append('0.2500000000000000e-005')                # 23 free surface smoothing (Lapidus)
    prgl.append('1.0')                                    # 24 coef. convection
    prgl.append('1.0')                                    # 25 coef. gravity
    prgl.append('1.0')                                    # 26 coef. manning
    prgl.append('0.0')                                    # 27 coef. wind
    prgl.append('001')                                    # 28 coef. boundary integral
    prgl.append('1.0e-018')                               # 29 coef. penality
    prgl.append('1.0e-007')                               # 30 coef. Kt perturbation (~ 0.1*sqrt(eps_mach); eps_mach ~ 2.2e-16)
    prgl.append('1.0e-009')                               # 31 min   Kt perturbation (~ 0.1*sqrt(eps_mach); eps_mach ~ 2.2e-16)
    prgl.append('1.0')                                    # 32 coef. Kt relaxation
    prgl.append('1')                                      # 33 flag prel perturbation (0 = false, #=0 = true)
    prgl.append('1')                                      # 34 flag prno perturbation (0 = false, #=0 = true)
    
    par2prgl(args.input,args.output,prgl)

    if args.output:
        print(("output filename is {}".format(args.output)))
    
if __name__ == "__main__":
    main()